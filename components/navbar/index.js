import Logo from "../../public/assets/LOGO1.png";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState, useEffect } from "react";
import style from "../../styles/navbar.module.css";
import Image from "next/image";

export default function Navbar() {
  const [auth, setAuth] = useState("");
  const router = useRouter();

  useEffect(() => {
    // Perform localStorage action
    const auth = localStorage.getItem("token");
    setAuth(auth);
  }, []);

  const logout = () => {
    localStorage.clear();
    router.replace({ pathname: "/login" });
  };

  const handleClick = (path) => {
    router.replace({ pathname: `${path}` });
  };

  return (
    <header className="header">
      <Image
        src={Logo}
        onClick={() => (auth ? handleClick("/home") : handleClick("/"))}
        className={style.image}
      ></Image>
      <nav>
        <Link href="/about" className={style.navA}>
          Our Story
        </Link>
        <Link href="/gamelist" className={style.navA}>
          Games
        </Link>
        <Link href="/support" className={style.navA}>
          Support
        </Link>
        {auth ? (
          <>
            <a href="/profile" className={style.navA}>
              Profile
            </a>
            <button className={style.navbtn}>
              <Link onClick={logout} href="/login" className={style.buttona}>
                Logout
              </Link>
            </button>
          </>
        ) : (
          <>
            {" "}
            <Link href="/register" className={style.navA}>
              Sign Up
            </Link>
            <button className={style.navbtn}>
              <Link href="/login" className={style.buttona}>
                Login
              </Link>
            </button>
          </>
        )}
      </nav>
    </header>
  );
}
