import React from "react";
import style from "../../styles/footer.module.css";
import imgLogo from "../../public/assets/LOGO_FOOTER.png";
import Image from "next/image"

function Footer() {
  return (
    <div className={style.footercontainer}>
      <div className={style.footer}>
        <div>
          <Image src={imgLogo} alt="logo" className={style.footerimg}></Image>
        </div>
        <div>
          <p className={style.footertext}>play games @ Lame games © 2023 - lame games PA</p>
        </div>
      </div>
    </div>
  );
}

export default Footer;
