import React from "react";
import { Navigate } from "react-router-dom";
import { isLogin } from "../../utils";
import {useRouter} from "next/router"

const PrivateRoute = ({ children }) => {
  const router = useRouter()
  return isLogin(true) ? children : <router.replace href="/login" />;
};

export default PrivateRoute;
