import React from "react";
import styles from "../../../styles/rps_scoreBox.module.css";

const scoreBox = ({ score }) => {
  return (
    <div className={styles.score_box}>
      <span>Score</span>
      <div className={styles.score_box__score}>{score}</div>
    </div>
  );
};

export default scoreBox;
