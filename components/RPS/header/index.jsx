import React from "react";
import styles from "../../../styles/rps_header.module.css";

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.text}>
        <span>Rock Paper Scissors</span>
      </div>
    </div>
  );
};

export default Header;
