import React, { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";
import Triangle from "../../../public/assets/bg-triangle.svg";
import styles from "../../../styles/rps_play.module.css";
import Header from "../header";
import ScoreBox from "../scoreBox";
import Navbar from "../../../components/navbar/index";

const Play = () => {
  const router = useRouter();
  const { score } = router.query;
  const [scores, setScores] = useState(0);

  console.log("play_score:" + scores);

  const handleLinkClick = (choice) => {
    console.log("choice:" + choice);
  };

  const initialScore = (score) => {
    if (score == undefined) {
      setScores(0);
    } else {
      setScores(score);
    }
  };

  useEffect(() => {
    initialScore(score);
  }, []);

  return (
    <React.Fragment>
      <Navbar />
      <Header />
      <div className={styles.container_games}>
        <ScoreBox score={scores} />
        <div className={styles.gamesContent}>
          <div className={styles.play}>
            <Image src={Triangle} alt="" className={styles.triangle} />
            <div className={styles.items}>
              <Link
                href={{
                  pathname: "/rps/game",
                  query: { choice: "paper", score: scores },
                }}
              >
                <div
                  data-id="paper"
                  onClick={() => handleLinkClick("paper")}
                  className={[styles.icon, styles.icon_paper].join(" ")}
                ></div>
              </Link>
              <Link
                href={{
                  pathname: "/rps/game",
                  query: { choice: "scissors", score: scores },
                }}
              >
                <div
                  data-id="scissors"
                  onClick={() => handleLinkClick("scissors")}
                  className={[styles.icon, styles.icon_scissors].join(" ")}
                ></div>
              </Link>
              <Link
                href={{
                  pathname: "/rps/game",
                  query: { choice: "rock", score: scores },
                }}
              >
                <div
                  data-id="rock"
                  onClick={() => handleLinkClick("rock")}
                  className={[styles.icon, styles.icon_rock].join(" ")}
                ></div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Play;
