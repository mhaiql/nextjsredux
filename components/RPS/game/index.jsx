import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import styles from "../../../styles/rps_game.module.css";
import Header from "../header";
import ScoreBox from "../scoreBox";
import Navbar from "../../../components/navbar/index";

const Game = () => {
  const router = useRouter();
  const { choice, score } = router.query;
  const [scores, setScores] = useState(parseInt(score));
  const [com, setCom] = useState("");
  const [playerWin, setPlayerWin] = useState("");
  const [counter, setCounter] = useState(3);

  console.log("game_choice: " + choice);
  console.log("game_score:" + score);

  const handleClick = () => {
    setCom("");
  };

  const newComPick = () => {
    const choices = ["rock", "paper", "scissors"];
    setCom(choices[Math.floor(Math.random() * 3)]);
  };

  useEffect(() => {
    newComPick();
  }, []);

  const Result = () => {
    if (choice === "rock" && com === "scissors") {
      setPlayerWin("win");
      setScores(scores + 3);
    } else if (choice === "rock" && com === "paper") {
      setPlayerWin("lose");
      setScores(scores - 1);
    } else if (choice === "scissors" && com === "paper") {
      setPlayerWin("win");
      setScores(scores + 3);
    } else if (choice === "scissors" && com === "rock") {
      setPlayerWin("lose");
      setScores(scores - 1);
    } else if (choice === "paper" && com === "rock") {
      setPlayerWin("win");
      setScores(scores + 3);
    } else if (choice === "paper" && com === "scissors") {
      setPlayerWin("lose");
      setScores(scores - 1);
    } else {
      setPlayerWin("draw");
      setScores(scores + 1);
    }
  };

  useEffect(() => {
    const timer =
      counter > 0
        ? setInterval(() => {
            setCounter(counter - 1);
          }, 1000)
        : Result();

    return () => {
      clearInterval(timer);
    };
  }, [counter, com]);

  return (
    <React.Fragment>
      <Navbar />
      <Header />
      <div className={styles.container_games}>
        <ScoreBox score={scores} />
        <div className={styles.gamesContent}></div>
        <div className={styles.game}>
          <div className={styles.game_you}>
            <span className={styles.text}>You Picked</span>
            <div
              className={[
                styles.icon,
                styles[`icon_${choice}`],
                styles[`${playerWin === "win" ? `icon_${choice}_winner` : ""}`],
              ].join(" ")}
            ></div>
          </div>
          {playerWin === "win" && (
            <div className={styles.game_play}>
              <span className={styles.text_win}>You Win</span>
              <Link
                href={{
                  pathname: "/rps",
                  query: { score: scores },
                }}
                className={styles.play_again}
                onClick={() => handleClick()}
              >
                Play Again
              </Link>
            </div>
          )}
          {playerWin === "lose" && (
            <div className={styles.game_play}>
              <span className={styles.text_lose}>You Lose</span>
              <Link
                href={{
                  pathname: "/rps",
                  query: { score: scores },
                }}
                className={styles.play_again}
                onClick={() => handleClick()}
              >
                Play Again
              </Link>
            </div>
          )}
          {playerWin === "draw" && (
            <div className={styles.game_play}>
              <span className={styles.text}>Draw</span>
              <Link
                href={{
                  pathname: "/rps",
                  query: { score: scores },
                }}
                className={styles.play_again}
                onClick={() => handleClick()}
              >
                Play Again
              </Link>
            </div>
          )}

          <div className={styles.game_com}>
            <span className={styles.text}>The Com Picked</span>
            {counter === 0 ? (
              <div
                className={[
                  styles.icon,
                  styles[`icon_${com}`],
                  styles[`${playerWin === "lose" ? `icon_${com}_winner` : ""}`],
                ].join(" ")}
              ></div>
            ) : (
              <div className={styles.counter}>{counter}</div>
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Game;
