import { useState, useEffect } from "react";
import * as React from "react";
import {
  Card,
  Box,
  CardActions,
  Typography,
  CardContent,
  CardMedia,
  Button,
  Grid,
} from "@mui/material";
import imgBp from "../../public/assets/Home_Page_Big_Picture.jpg";
import imgRps from "../../public/assets/RPS.png";
import imgNba from "../../public/assets/NBA2K23.jpg";
import imgVal from "../../public/assets/VALORANT.jpg";
import Navbar from "../../components/navbar/index";
import Footer from "../../components/Footer/index";
import homeStyles from "../../styles/home.module.css";
import Image from "next/image";
import { useRouter } from "next/router";

const HomePage = () => {
  const [user, setUser] = useState("");
  const router = useRouter();

  useEffect(() => {
    const user = localStorage.getItem("username");
    setUser(user);
    const token = localStorage.getItem("token");
    if (token == null) {
      router.replace("/login");
    }
  }, []);

  return (
    <React.Fragment>
      <Navbar />
      <div className={homeStyles.container}>
        <Card
          sx={{
            display: "flex",
            width: 1,
            borderRadius: "10px",
          }}
        >
          <Box sx={{ width: "50%" }}>
            <CardContent>
              <Typography
                gutterBottom
                variant="h2"
                component="div"
                sx={{ width: 1 }}
                style={{ fontWeight: "bold", color: "#5551FF" }}
              >
                <span style={{ color: "black" }}>Hi,</span> {user}
              </Typography>
              <p>Start playing games on our website now!!</p>
            </CardContent>
            <CardActions style={{ marginBottom: "20px", marginTop: "30px" }}>
              <Button
                size="md"
                style={{
                  frontWeight: "bold",
                  backgroundColor: "black",
                  color: "white",
                  marginLeft: "10px",
                }}
              >
                about us
              </Button>
            </CardActions>
          </Box>
          <Image src={imgBp} className={homeStyles.img1} />
        </Card>
      </div>
      <div className={homeStyles.container2}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Card sx={{ width: "100%" }}>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h4"
                  style={{
                    fontWeight: "bold",
                    textOverflow: "ellipsis",
                    overflow: "hidden",
                    whiteSpace: "nowrap",
                  }}
                  component="div"
                >
                  Rock, Paper & Scissors
                </Typography>
              </CardContent>
              <Image alt="" src={imgRps} className={homeStyles.img2} />
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card sx={{ maxWidth: "100%" }}>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h4"
                  style={{ fontWeight: "bold" }}
                  component="div"
                >
                  NBA 2K23
                </Typography>
              </CardContent>
              <Image
                component="img"
                alt=""
                src={imgNba}
                className={homeStyles.img3}
              ></Image>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card sx={{ maxWidth: "100%" }}>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h4"
                  style={{ fontWeight: "bold" }}
                  component="div"
                >
                  VALORANT
                </Typography>
              </CardContent>
              <Image
                component="img"
                alt=""
                src={imgVal}
                className={homeStyles.img4}
              ></Image>
            </Card>
          </Grid>
        </Grid>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default HomePage;
