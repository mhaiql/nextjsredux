import * as React from "react";
// import "./support.css";
import SupportIMG from "../../public/assets/Support.svg";
import Image from "next/image";
import Navbar from "../../components/navbar/index";
import Footer from "../../components/Footer/index";
import supportStyles from "../../styles/support.module.css";
import { Typography } from "@mui/material";

const Support = () => {
  return (
    <React.Fragment>
      <Navbar />
      <div className={supportStyles.containerAbout}>
        <div className={supportStyles.containerText}>
          <Typography variant="h3">Having any Troubles?</Typography>
          <Typography variant="h6" className={supportStyles.text2}>
            If you have any problems, feel free to contact us !
          </Typography>
          <Typography variant="h6" className={supportStyles.text3}>
            lamegames@support.com
          </Typography>
          <Typography variant="h6">021-999-889</Typography>
          <Typography variant="h6" className={supportStyles.text4}>
            Hope that helps!
          </Typography>
        </div>
        <div className={supportStyles.containerImage}>
          <Image className={supportStyles.aboutus} src={SupportIMG} alt="about us" />
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default Support;
