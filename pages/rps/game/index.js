import Game from "../../../components/RPS/game";
import { useRouter } from "next/router";
import { useEffect } from "react";

const Games = () => {
  const router = useRouter()

  useEffect(() => {
    const token = localStorage.getItem("token");
    if(token==null){router.replace("/gamedetails/1")}
  }, []);
  return <Game />;
};

export default Games;
