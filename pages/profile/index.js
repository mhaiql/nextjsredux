import {
  TextField,
  Button,
  CardContent,
  Alert,
  makeStyles,
} from "@mui/material";
import { useState, useEffect, Redirect } from "react";
import {useRouter} from "next/router"
import LogoImg from "../../public/assets/LOGO1.png";
import ProfileImg from "../../public/assets/profile1.png";
import axios from "axios";
import * as React from "react";
import Image from "next/image";
import Navbar from "../../components/navbar/index";
import Footer from "../../components/Footer/index";
import style from "../../styles/profile.module.css";

const Profile = () => {
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const [token, setToken] = useState("")
  
  useEffect(()=> {
    const username = localStorage.getItem("username");
    const email = localStorage.getItem("email");
    const token = localStorage.getItem("token");
    if(token==null){router.replace("/login")}
    setUsername(username)
    setEmail(email)
    setToken(token)
  })

  const [NewUsername, setNewUsername] = useState("");
  const [NewEmail, setNewEmail] = useState("");
  const [NewPassword, setNewPassword] = useState("");
  const [OldPassword, setOldPassword] = useState("");
  const [success, setSuccess] = useState("");
  const [error, setError] = useState("");
  const [editMode, setEditMode] = useState(false);
  const [deleteMode, setDelMode] = useState(false);
  const router = useRouter()

  const handleUpdateProfile = () => {
    const payload = { NewUsername, NewEmail, OldPassword, NewPassword };
    axios
      .put(`http://localhost:5000/user/updateacc`, payload, {
        headers: { Authorization: `${token}` },
      })
      .then((res) => {
        setUsername(res.data.NewUsername);
        setEmail(res.data.NewEmail);
        setOldPassword("");
        setNewPassword("");
        setSuccess(res.data.message);
        localStorage.setItem("username", res.data.data.username);
        localStorage.setItem("email", res.data.data.email);
        setEditMode(false);
        console.log(username)
        console.log(email)
        console.log(NewUsername)
        console.log(NewEmail)

        setTimeout(() => {
          setSuccess("");
        }, 2000);
      })
      .catch((err) => {
        setError(err.response.data.message);
        setError(err.response.data.msg);
        setTimeout(() => {
          setError("");
        }, 2000);
      });
  };

  const handleEditProfile = () => {
    setNewUsername(username)
    setNewEmail(email)
    setEditMode(true);
  };

  const handleCancelEdit = () => {
    setEditMode(false);
  }

  const handleCancelDel = () => {
    setDelMode(false);
  }

  const handleDelete = () => {
    setDelMode(true);
  }

  const handleDeleteProfile = () => {
    axios
      .delete(`http://localhost:5000/user/deleteacc`, {
        headers: { Authorization: `${token}` },
      })
      .then((res) => {
        setSuccess(res.data.message);
        localStorage.clear();

        console.log(setSuccess);

        setTimeout(() => {
          setSuccess("");
        }, 2000);
        setTimeout(() => {
          router.replace("/login");
        }, 2000);
      })
      .catch((err) => {
        setError(err.response.data.msg);

        setTimeout(() => {
          setError("");
        }, 2000);
      });
  };

  return (
    <React.Fragment>
      <Navbar/>
      <div className="container">
        <div className={style.containerprofile}>
          <div className={style.container35}>
            <CardContent
              style={{
                paddingBottom: "100px",
                fontSize: "20px",
                width: "400px",
                justifyContent: "center",
              }}
            >
              <div className={style.logoo}>
                <Image src={LogoImg} className={style.logo} alt="" width="180px"></Image>
              </div>
              <h1 className={style.title}>Profile</h1>
              {editMode ? (
                  <div>
                    <TextField
                    style={{ marginTop: "15px" }}
                    variant="standard"
                    fullWidth
                    value={NewUsername}
                    onChange={(e) => setNewUsername(e.target.value)}
                    disabled={!editMode}
                  />
                  <TextField
                    style={{ marginTop: "15px" }}
                    variant="standard"
                    fullWidth
                    value={NewEmail}
                    onChange={(e) => setNewEmail(e.target.value)}
                    disabled={!editMode}
                  />
                  <TextField
                    style={{ marginTop: "15px" }}
                    label="OldPassword"
                    type="password"
                    variant="standard"
                    fullWidth
                    value={OldPassword}
                    onChange={(e) => setOldPassword(e.target.value)}
                    disabled={!editMode}
                  />
                  <TextField
                    style={{ marginTop: "15px" }}
                    label="NewPassword"
                    type="password"
                    variant="standard"
                    fullWidth
                    value={NewPassword}
                    onChange={(e) => setNewPassword(e.target.value)}
                    disabled={!editMode}
                  />
                  <Button
                    style={{
                      color: "white",
                      backgroundColor: "black",
                      borderRadius: "15px",
                      marginTop: "30px",
                      width: "400px",
                    }}
                    onClick={handleUpdateProfile}
                  >
                    Edit Profile
                  </Button>
                  <Button
                    style={{
                      color: "white",
                      backgroundColor: "black",
                      borderRadius: "15px",
                      marginTop: "30px",
                      width: "400px",
                    }}
                    onClick={handleCancelEdit}
                  >
                    Cancel Edit
                  </Button>
                  {deleteMode ? (
                  <div>
                    <p className={style.sure}>Are you sure want to delete this profile?</p>
                    <Button
                      style={{
                        color: "white",
                        backgroundColor: "black",
                        borderRadius: "15px",
                        marginTop: "30px",
                        width: "400px",
                      }}
                      onClick={handleCancelDel}
                    >
                      No, cancel it
                    </Button>
                    <Button
                      style={{
                        color: "white",
                        backgroundColor: "#FF6584",
                        borderRadius: "15px",
                        marginTop: "30px",
                        width: "400px",
                      }}
                      onClick={handleDeleteProfile}
                    >
                      Yes, Delete Account
                    </Button>
                  </div>
                  ) : (
                  <Button
                    style={{
                      color: "white",
                      backgroundColor: "#FF6584",
                      borderRadius: "15px",
                      marginTop: "30px",
                      width: "400px",
                    }}
                    onClick={handleDelete}
                  >
                    Delete Account
                  </Button>
                  )}
                </div>
              ) : (
                <div>
                  <TextField
                    style={{ marginTop: "15px" }}
                    variant="standard"
                    fullWidth
                    value={username}
                    disabled={!editMode}
                  />
                  <TextField
                    style={{ marginTop: "15px" }}
                    variant="standard"
                    fullWidth
                    value={email}
                    disabled={!editMode}
                  />
                  <Button
                    style={{
                      color: "white",
                      backgroundColor: "black",
                      borderRadius: "15px",
                      marginTop: "30px",
                      width: "400px",
                    }}
                    onClick={handleEditProfile}
                  >
                    Update Profile
                  </Button>
                </div>
              )}
              {success && (
                <Alert style={{ marginTop: "10px" }} severity="success">
                  {success}
                </Alert>
              )}
              {error && (
                <Alert style={{ marginTop: "10px" }} severity="error">
                  {error}
                </Alert>
              )}
            </CardContent>
          </div>
          <div className={style.container40}>
            <Image className={style.profileimg} src={ProfileImg} alt="profile" />
          </div>
        </div>
      </div>
      <Footer/>
    </React.Fragment>
  );
};

export default Profile;
