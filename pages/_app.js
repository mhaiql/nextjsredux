import '../styles/global.css'
import { AppProps } from 'next/app'
import Head from "next/head"

export default function App({ Component, pageProps }, AppProps) {
 
  return (
    <>
      <Head>
        <title>Lame Games</title>
        <link rel="shortcut icon" href="/favicon/favicon.png" />
      </Head>
      <Component {...pageProps} />
        <style jsx global>
            {`body {
                background: #e7f2f8;
                padding: 0;
                margin: 0;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
            }
            header {
                display: flex;
                align-items: center;
                justify-content: space-between;
                height: 80px;
                padding-right: 12%;
                padding-left: 12%;
            }
            `}
        </style>
    </>
  )
}