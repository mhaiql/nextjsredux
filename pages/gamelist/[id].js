import { useState, useEffect } from "react";
import * as React from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import Box from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import imgRPS from "../../public/assets/RPS.png";
import style from "../../styles/gamedetails.module.css";
import { Container } from "react-bootstrap";
import { useRouter } from "next/router";

const GameDetail = () => {
  const router = useRouter();
  const id = router.query.id;

  const [gamelist, setGamelist] = useState([]);

  const handleFetch = () => {
    axios.get("http://localhost:5000/games/").then((res) => {
      setGamelist(res);
    });
  };

  useEffect(() => {
    handleFetch();
  }, []);

  return (
    <div className={style.containergd}>
      <Card sx={{ display: "flex", width: 1, height: "450px" }}>
        <Box sx={{ maxWidth: 1 / 2 }}>
          <CardContent>
            <Typography
              gutterBottom
              variant="h2"
              component="div"
              sx={{ width: 1, ml: 1 }}
              style={{ fontWeight: "bold" }}
            >
              {gamelist?.data?.result[id].name}
            </Typography>
            <Typography
              gutterBottom
              variant="h6"
              component="div"
              sx={{ width: 0.9, ml: 1 }}
            >
              {gamelist?.data?.result[id].detail}
            </Typography>
          </CardContent>
          <CardActions style={{ marginBottom: "20px", marginTop: "30px" }}>
            <Button
              size="md"
              style={{
                fontWeight: "bold",
                backgroundColor: "black",
                color: "white",
                width: "40%",
              }}
              sx={{ ml: 1 }}
            >
              <a href="/rps">PLAY GAME</a>
            </Button>
          </CardActions>
        </Box>
        <CardMedia
          sx={{
            maxWidth: 1 / 2,
            backgroundSize: "cover",
            height: "inherit",
            width: "inherit",
          }}
          image={gamelist?.data?.result[id].image}
          title="game image"
          style={{ objectFit: "contain", borderRadius: "10px 0 0 10px" }}
        />
      </Card>
    </div>
  );
};

export default GameDetail;
