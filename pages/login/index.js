import {
  TextField,
  Button,
  CardContent,
  Alert,
  makeStyles,
} from "@mui/material";
import { useState, useEffect, Redirect } from "react";
import style from "../../styles/login.module.css"
import LogoImg from "../../public/assets/LOGO1.png";
import LoginImg from "../../public/assets/login.png";
import Image from 'next/image';
import axios from "axios";
import { useRouter } from "next/router";

const Login = () => {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");
  const [success, setSuccess] = useState("");
  const [error, setError] = useState("");
  const router = useRouter();

  useEffect(() => {
    const token = localStorage.getItem("token");
    if(token!==null){router.replace("/home")}
  }, []);

  const handleLogin = () => {
    const payload = { user, password };
    axios
      .post("http://localhost:5000/auth/login", payload)
      .then((res) => {
        localStorage.setItem("id", res.data.data.id);
        localStorage.setItem("username", res.data.data.username);
        localStorage.setItem("email", res.data.data.email);
        localStorage.setItem("token", res.data.data.token);
        console.log(res.data.data.token);
        setSuccess(res.data.status);

        setTimeout(() => {
          setSuccess("");
          router.replace({pathname: "/home"});
        }, 2000);
      })

      .catch((err) => {
        setError(err.response.data.msg);
        setTimeout(() => {
          setError("");
        }, 2000);
      });
  };

  const handleKeypress = (e) => {
    if (e.keyCode === 13) {
      handleLogin();
    }
  };

  return (
    <div className={style.appcontainer}>
      <CardContent
        style={{
          paddingBottom: "100px",
          fontSize: "20px",
          width: "400px",
          justifyContent: "center",
        }}
      >
        <div className={style.logoo}>
          <Image src={LogoImg} alt="" width="180px" className={style.logo}></Image>
        </div>
        <h1 className={style.title}>Login</h1>
        <p className={style.paragraph}>Login to access all the features</p>
        <TextField
          style={{ marginTop: "15px" }}
          label="Email / Username"
          variant="standard"
          width="20px"
          fullWidth
          value={user}
          onChange={(e) => setUser(e.target.value)}
          onKeyUp={handleKeypress}
        />
        <TextField
          style={{ marginTop: "15px" }}
          label="Password"
          type="password"
          variant="standard"
          fullWidth
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          onKeyUp={handleKeypress}
        />
        <Button
          style={{
            color: "white",
            backgroundColor: "black",
            borderRadius: "15px",
            marginTop: "30px",
            width: "400px",
          }}
          onClick={handleLogin}
        >
          Log in
        </Button>
        <div className={style.signin}>
          <span>
            doesn't have an account?{" "}
            <a href="/register">register here</a>
          </span>
        </div>
        {success && (
          <Alert style={{ marginTop: "10px" }} severity="success">
            {success}
          </Alert>
        )}
        {error && (
          <Alert style={{ marginTop: "10px" }} severity="error">
            {error}
          </Alert>
        )}
      </CardContent>
      <div className="right">
        <Image src={LoginImg} alt="" className={style.rightimg}></Image>
      </div>
    </div>
  );
};

export default Login;
